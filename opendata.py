#網路連線
# import urllib.request as request
# src="https://www.ntu.edu.tw/"
# with request.urlopen(src) as response:
#     data = response.read().decode("utf-8") #取得台灣大學網頁原始碼 並用亞洲代碼
# print(data)

#串接、擷取公開資料 (使用API存取資料)
import urllib.request as request
import json 
#高雄市政府opendata
src = "https://data.kcg.gov.tw/dataset/a98754a3-3446-4c9a-abfc-58dc49f2158c/resource/48d4dfc4-a4b2-44a5-bdec-70f9558cd25d/download/yopendata1070622opendatajson-1070622.json"
with request.urlopen(src) as response:
    data = json.load(response)

#電動機車充電站地址
addresses = [] #宣告空list
for info in data:
    addresses.append(info["Address"])
#print(addresses)
with open("addresses_of_chargestation.txt", mode="w", encoding="utf-8") as file:
    for address in addresses:
        file.write(address+"\n")